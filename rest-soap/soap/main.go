package main

import (
	"bytes"
	"crypto/tls"
	"encoding/xml"
	"fmt"
	"log"
	"net/http"
	"strings"
)

type Envelope struct {
	XMLName xml.Name `xml:"Envelope"`
	Body    struct {
		NumberToWordsResponse struct {
			NumberToWordsResult string `xml:"NumberToWordsResult"`
		} `xml:"NumberToWordsResponse"`
	} `xml:"Body"`
}

func main() {
	var number int

	fmt.Print("Insert number: ")
	fmt.Scan(&number)

	data := []byte(strings.TrimSpace(fmt.Sprintf(
		`<?xml version="1.0" encoding="utf-8"?>
			<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  				<soap:Body>
    				<NumberToWords xmlns="http://www.dataaccess.com/webservicesserver/">
      					<ubiNum>%d</ubiNum>
    				</NumberToWords>
  				</soap:Body>
			</soap:Envelope>`, number)))

	result, err := TestSoap(data)
	if err != nil {
		return
	}

	fmt.Println("the number in letters is: ", result)
}

func TestSoap(data []byte) (string, error) {
	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	url := "https://www.dataaccess.com/webservicesserver/NumberConversion.wso"
	request, err := http.NewRequest("POST", url, bytes.NewReader(data))
	if err != nil {
		return "", err
	}

	request.Header.Set("Content-type", "text/xml; charset=utf-8")

	response, err := client.Do(request)
	if err != nil {
		log.Fatal("Error on dispatching request. ", err.Error())
		return "", err
	}

	defer response.Body.Close()

	result := new(Envelope)
	err = xml.NewDecoder(response.Body).Decode(result)
	if err != nil {
		return "", err
	}

	return result.Body.NumberToWordsResponse.NumberToWordsResult, nil
}
