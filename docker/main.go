package main

import "fmt"

type Person struct {
	Name string
	Age  int
}

func main() {
	p := Person{
		Name: "ernesto",
		Age:  22,
	}

	fmt.Printf("Your name is: %s, age: %d", p.Name, p.Age)
}
