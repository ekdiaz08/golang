package main

import (
	"fmt"
	"math"
)

func main() {
	values := []string{"ernesto", "jose", "andre"}
	arrayShow(values)
	fmt.Println("value: ", values[:3])
}

func arrayShow(value []string) {
	for i := len(value) - 1; i >= 0; i-- {
		fmt.Println(value[i])
	}
}

func exercise1(value string) string {
	runes := []rune(value)
	for i, j := 0, len(runes)-1; i < len(runes)/2; i, j = i+1, j-1 {
		if math.Abs(float64(runes[i]-runes[i+1])) != math.Abs(float64(runes[j]-runes[j-1])) {
			return "good"
		}
	}
	return "bad"
}
