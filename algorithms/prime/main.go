package main

import (
	"fmt"
	"math"
)

func main() {
	var number int
	fmt.Print("insert your number: ")
	fmt.Scan(&number)

	ok := isPrime(number)
	fmt.Println("is prime: ", ok)
}

func isPrime(number int) bool {
	if number == 2 {
		return true
	}

	if math.Mod(float64(number), 2) == 0 {
		return false
	}

	limit := int(math.Ceil(math.Sqrt(float64(number))))
	for i := 3; i <= limit; i += 2 {
		if math.Mod(float64(number), float64(i)) == 0 {
			return false
		}
	}

	return true
}
