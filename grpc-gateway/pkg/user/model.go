package user

import "errors"

type GetAll struct {
	ID       string
	Name     string
	Age      int
	Email    string
	Password string
}

func (g GetAll) CheckPassword() error {
	if len(g.Password) < 6 {
		return errors.New("your password is very easy")
	}
	return nil
}
