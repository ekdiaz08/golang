package user

import "context"

type Service interface {
	GetAll(ctx context.Context) ([]GetAll, error)
	GetByID(ctx context.Context, id string) (*GetAll, error)
	Add(ctx context.Context, req *GetAll) (id string, err error)
}

type service struct {
	repo Repository
}

func NewService(repo Repository) Service {
	return &service{
		repo: repo,
	}
}

func (s service) GetAll(ctx context.Context) ([]GetAll, error) {
	users, err := s.repo.GetAll(ctx)
	if err != nil {
		return nil, err
	}

	return users, nil
}

func (s service) GetByID(ctx context.Context, id string) (*GetAll, error) {
	user, err := s.repo.GetByID(ctx, id)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (s service) Add(ctx context.Context, req *GetAll) (id string, err error) {
	userID, err := s.repo.Add(ctx, req)
	if err != nil {
		return "", err
	}

	return userID, nil
}
