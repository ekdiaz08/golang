package user

import (
	"context"

	"google.golang.org/protobuf/types/known/emptypb"
)

type Handler struct {
	UnimplementedUserServiceServer
	ser Service
}

func NewHandler(ser Service) UserServiceServer {
	return &Handler{
		ser: ser,
	}
}

func (h Handler) GetAll(ctx context.Context, _ *emptypb.Empty) (*GetAllResponse, error) {
	users, err := h.ser.GetAll(ctx)
	if err != nil {
		return nil, err
	}
	l := len(users)
	res := make([]*GetUser, 0)
	for i := 0; i < l; i++ {
		us := users[i]
		res[i] = &GetUser{
			Name:     us.Name,
			Age:      int64(us.Age),
			Email:    us.Email,
			Password: us.Password,
		}
	}

	return &GetAllResponse{
		Result: res,
	}, nil
}

func (h Handler) Add(ctx context.Context, req *AddRequest) (*AddResponse, error) {
	userID, err := h.ser.Add(ctx, &GetAll{
		ID:       req.GetId(),
		Name:     req.GetName(),
		Age:      int(req.GetAge()),
		Email:    req.GetEmail(),
		Password: req.GetPassword(),
	})

	if err != nil {
		return nil, err
	}
	return &AddResponse{
		Id: userID,
	}, nil
}
