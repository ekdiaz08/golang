package user

import (
	"context"
	"database/sql"
	"log"
)

type Repository interface {
	GetAll(ctx context.Context) ([]GetAll, error)
	GetByID(ctx context.Context, id string) (*GetAll, error)
	Add(ctx context.Context, req *GetAll) (id string, err error)
}

type postgres struct {
	db *sql.DB
}

func NewPostgres(db *sql.DB) Repository {
	return &postgres{
		db: db,
	}
}

func (p postgres) GetAll(ctx context.Context) ([]GetAll, error) {
	q := `SELECT "name", age, email, password FROM "user";`

	res := make([]GetAll, 0)
	row, err := p.db.QueryContext(ctx, q)
	if err != nil {
		log.Printf("error %v", err)
		return nil, err
	}

	for row.Next() {
		var user GetAll
		err = row.Scan(
			&user.Name,
			&user.Age,
			&user.Email,
			&user.Password,
		)
		if err != nil {
			continue
		}

		res = append(res, user)
	}

	return res, nil
}

func (p postgres) GetByID(ctx context.Context, id string) (*GetAll, error) {
	q := `SELECT name, age, email, password FROM "user" WHERE id=$1;`

	var user GetAll
	err := p.db.QueryRowContext(ctx, q, id).Scan(
		&user.Name,
		&user.Age,
		&user.Email,
		&user.Password,
	)
	if err != nil {
		return nil, err
	}

	return &user, nil

}

func (p postgres) Add(ctx context.Context, req *GetAll) (id string, err error) {
	q := `INSERT INTO "user" (name, age, email, password) VALUES ($1, $2, $3, $4);`

	stmt, err := p.db.PrepareContext(ctx, q)
	if err != nil {
		log.Printf("error %v", err)
		return "", err
	}

	defer stmt.Close()

	err = stmt.QueryRowContext(ctx,
		req.Name,
		req.Age,
		req.Email,
		req.Password,
	).Scan(&id)
	if err != nil {
		log.Printf("error %v", err)
		return "", err
	}

	return id, nil
}
