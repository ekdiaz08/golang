CREATE TABLE "user" (
  id        serial,
  "name"    varchar(100),
  age       integer,
  email     varchar(100),
  password  varchar(50),
  CONSTRAINT user_id_pk PRIMARY KEY (id)
);