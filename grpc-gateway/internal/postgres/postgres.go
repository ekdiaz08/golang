package postgres

import (
	"database/sql"
)

type Client struct {
	*sql.DB
}

func NewClientPostgres(source string) (*Client, error) {
	db, err := sql.Open("postgres", source)
	if err != nil {
		return nil, err
	}

	return &Client{
		db,
	}, nil
}
