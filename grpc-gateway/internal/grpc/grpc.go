package grpc

import (
	"fmt"
	"net"

	"gitlab.com/ekdiaz08/golang/grpc-gateway/pkg/user"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func Server(address string, serv user.Service) error {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%s", address))
	if err != nil {
		return err
	}

	grpcServer := grpc.NewServer()

	ha := user.NewHandler(serv)
	user.RegisterUserServiceServer(grpcServer, ha)

	reflection.Register(grpcServer)

	fmt.Println("GRPC is ready to handle requests ", lis.Addr().String())
	if err = grpcServer.Serve(lis); err != nil {
		return err
	}

	return nil
}
