package main

import (
	"errors"
	"fmt"
	"os"
	"os/signal"

	_ "github.com/lib/pq"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"

	"gitlab.com/ekdiaz08/golang/grpc-gateway/internal/grpc"
	"gitlab.com/ekdiaz08/golang/grpc-gateway/internal/postgres"
	"gitlab.com/ekdiaz08/golang/grpc-gateway/pkg/user"
)

const sourceURL = "file://grpc-gateway/migrations"

func main() {
	databaseURI := fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", "user",
		"password", "localhost:5432", "user_db")
	client, err := postgres.NewClientPostgres(databaseURI)
	if err != nil {
		os.Exit(1)
	}

	err = NewMigrate(databaseURI)
	if err != nil {
		os.Exit(1)
	}

	userRepo := user.NewPostgres(client.DB)
	userService := user.NewService(userRepo)

	go func() {
		if err := grpc.Server("9090", userService); err != nil {
			os.Exit(1)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c

	fmt.Printf("Service gracefully shut down")
	os.Exit(0)
}

func NewMigrate(databaseURI string) error {
	m, err := migrate.New(
		sourceURL,
		databaseURI,
	)
	if err != nil {
		return err
	}

	if err = m.Up(); err != nil && !errors.Is(err, migrate.ErrNoChange) {
		return err
	}

	return nil
}
