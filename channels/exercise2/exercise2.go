package exercise2

import (
	"fmt"
	"sort"
	"strings"
	"sync"
)

func Order(items ...string) {
	if len(items) == 0 {
		return
	}

	wg := &sync.WaitGroup{}
	wg.Add(1)
	go func() {
		for _, item := range items {
			name := strings.Split(item, ",")
			sort.Sort(sort.StringSlice(name))
			fmt.Println(name)
		}
		wg.Done()
	}()

	wg.Wait()
}
