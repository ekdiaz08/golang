package main

import (
	"sync"

	"gitlab.com/ekdiaz08/golang/channels/exercise1"
	"gitlab.com/ekdiaz08/golang/channels/exercise2"
)

func main() {
	TestExercise2()
}

func TestExercise1() {
	wg := &sync.WaitGroup{}
	uuidChan := make(chan string)
	uuidFakeChan := make(chan string)
	closeChan := make(chan int)

	wg.Add(3)
	go exercise1.GenerateUUID(wg, uuidChan, closeChan)
	go exercise1.GenerateFakeUUID(wg, uuidFakeChan, closeChan)
	go exercise1.LoggerUUID(wg, uuidChan, uuidFakeChan, closeChan)

	wg.Wait()
}

func TestExercise2() {
	mapData := "Rosa, Jose, Pedro, Daniel, Ana, Antonia, Julio"

	exercise2.Order(mapData)
}
