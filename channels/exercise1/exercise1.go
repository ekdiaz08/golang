package exercise1

import (
	"fmt"
	"sync"

	"github.com/google/uuid"
)

func GenerateFakeUUID(wg *sync.WaitGroup, idFake chan<- string, closedChan chan<- int) {
	for i := 0; i < 10; i++ {
		uuidGen := uuid.New()
		idFake <- fmt.Sprintf("%d -> %s", i+1, uuidGen.String())
	}

	close(idFake)
	closedChan <- 1

	wg.Done()
}

func GenerateUUID(wg *sync.WaitGroup, id chan<- string, closedChan chan<- int) {
	for i := 0; i < 20; i++ {
		uuidGen := uuid.New()
		id <- fmt.Sprintf("%d -> %s", i+1, uuidGen.String())
	}

	close(id)
	closedChan <- 1

	wg.Done()
}

func LoggerUUID(wg *sync.WaitGroup, id <-chan string, idFake <-chan string, closedChan chan int) {
	closeCounter := 0
	for {
		select {
		case idO, ok := <-id:
			if ok {
				fmt.Println("id original", idO)
			}
		case idf, ok := <-idFake:
			if ok {
				fmt.Println("id fake", idf)
			}
		case count, ok := <-closedChan:
			if ok {
				closeCounter += count
			}
		}
		if closeCounter == 2 {
			close(closedChan)
			break
		}
	}

	wg.Done()
}
