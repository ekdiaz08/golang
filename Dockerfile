# Type your Dockerfile here
ARG NODE_VERSION=13-alpine

FROM node:$NODE_VERSION

WORKDIR /usr/src/app

VOLUME /var/lib/project

COPY package-lock.json .
COPY . ./project

RUN npm install

EXPOSE 8080

CMD ["node", "server.js"]